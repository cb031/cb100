#include <stdio.h>
int input()
{
    int n;
    printf("Enter the number of elements of the array: ");
    scanf("%d",&n);
    printf("\n");
    return n;
}
int main()
{
	int n,i,temp,large_pos,small_pos;
	int large,small;
	n=input();
	int a[n];
	printf("Enter the numbers:");
	for (i=0;i<n;i++)
	{
		printf("\na[%d]=",i);
		scanf("%d",&a[i]);
	}
	large=a[0];
	small=a[0];
    large_pos=small_pos=0;
	for (i=0;i<n;i++)
	{
		if (a[i]<small)
		{
			small=a[i];
			small_pos=i;
		}
		if (a[i]>large)
		{
			large=a[i];
			large_pos=i;
		}
	}
	printf("The largest number is : %d\n",large);
	printf("The smallest number is : %d\n",small);
	printf("Before interchanging the largest and smallest numbers,the order is:");
	for(i=0;i<n;i++)
    {
        printf("\n%d",a[i]);
    }
	temp=a[large_pos];
	a[large_pos]=a[small_pos];
	a[small_pos]=temp;
	printf("\nAfter interchanging the largest and smallest elements of the array,the order is:");
	for (i=0;i<n;i++)
	{
		printf("\n%d",a[i]);
	}
	return 0;
}
