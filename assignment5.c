#include <stdio.h>

int main()
{
    int n;
    printf("Enter the number of elements\n");
    scanf("%d",&n);
    int a[n];
    printf("Enter the elements\n");
    for(int i=0;i<n;i++)
        scanf("%d",&a[i]);
    float sum=0;
    for(int i=0;i<n;i++)
        sum=sum+a[i];
    float m=sum/n;
    printf("The elements which are greater than the average are\n");
    for(int i=0;i<n;i++)
    {
        if(a[i]>m)
        printf("%d\n",a[i]);
    }

    return 0;
}
