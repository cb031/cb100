#include <stdio.h>
int main ()
{
 char st1[300], st2[100], i, j;
 printf("This program concatenates 2 given strings.\n");
 printf ("Enter first string:\n");
 scanf ("%s", st1);
 printf ("Enter second string:\n");
 scanf ("%s", st2);
 for (i = 0; st1[i] != '\0'; ++i);
 for (j = 0; st2[j] != '\0'; ++j, ++i)
 {
 st1[i] = st2[j];
 }
 st1[i] = '\0';
 printf ("\nOutput: %s", st1);
}
